package org.beardfish

interface LRU<K,V> {
    val capacity: Int
    operator fun get(key: K): V?
    operator fun set(key: K, value: V)
    fun put(key: K, value: V)
    fun size(): Int;
}