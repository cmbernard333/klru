package org.beardfish

class LRUImpl<K,V>(
    override val capacity: Int,
    private val cache: MutableMap<K, LRUEntry<K, V>> = mutableMapOf<K,LRUEntry<K, V>>()) : LRU<K,V> {

    private var lruHead: LRUEntry<K, V>? = null
    private var lruTail: LRUEntry<K, V>? = null

    class LRUEntry<K, V>(
        val key: K,
        val value: V,
        var prev: LRUEntry<K, V>? = null, var next: LRUEntry<K, V>? = null) {}

    override fun get(key: K): V? {
        // 1. check if the value exists
        // 2. update its position in the lru list
        // 3. return if it's available
        val lruEntry: LRUEntry<K, V>? = this.cache[key]
        if (lruEntry != null && this.cache.size > 1) {
            // check if this is the head
            if (this.lruHead === lruEntry) {
                this.lruHead = this.lruHead?.next
            }
            // move value to the end of the list
            val prev = lruEntry.prev
            prev?.next = lruEntry.next
            this.lruTail?.next = lruEntry
            this.lruTail = lruEntry
        }
        return lruEntry?.value
    }

    override fun set(key: K, value: V) {
        put(key, value)
    }

    override fun put(key: K, value: V) {
        val newEntry: LRUEntry<K, V> = LRUEntry(key, value, prev = lruTail)
        if (!this.cache.containsKey(key) && this.cache.size + 1 > this.capacity) {
            // 1. evict the head
            // 2. set new head
            this.cache.remove(lruHead?.key)
            this.lruHead = this.lruHead?.next
            this.lruHead?.prev = null
        }
        // first entry = null; otherwise this should never be null again
        if (this.cache.size + 1 == 1) {
            this.lruHead = newEntry
        }
        this.cache[key] = newEntry
        // update the tail
        val prevTail = this.lruTail
        this.lruTail = newEntry
        prevTail?.next = newEntry
    }

    override fun size(): Int {
        return this.cache.size
    }
}