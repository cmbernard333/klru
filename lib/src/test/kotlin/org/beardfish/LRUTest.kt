package org.beardfish

import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertNull
import kotlin.test.assertTrue

class LRUTest {
    @Test fun testEmptyLRUReturnsZero() {
        val lruCache: LRU<Int, String> = LRUImpl(10)
        assertTrue(lruCache.size() == 0)
    }
    @Test fun testLRUSetAndGet() {
        val lruCache: LRU<Int, String> = LRUImpl(10)
        lruCache[0] = "Bob"
        assertEquals("Bob", lruCache[0])
    }
    @Test fun testLRUCapacityAndEvict() {
        val lruCache: LRU<Int, Int> = LRUImpl(5)
        for (i in 0..5) {
            lruCache[i] = i
        }
        assertNull(lruCache[0])
        assertEquals(5, lruCache.size())
        for (i in 1..5) {
            assertEquals(i, lruCache[i])
        }
    }
    @Test fun testLRUEvictAfterGet() {
        val lruCache: LRU<Int, Int> = LRUImpl(3)
        for (i in 1..3) {
            lruCache[i] = i
        }
        // access 1 to move it to the front
        val lruEntry = lruCache.get(1)
        // add another value to evict 2
        lruCache[4] = 4
        assertNull(lruCache[2])
    }
}